#pragma once

#include <io/wav_reader.hpp>
#include <vector>

namespace core
{
	struct encoding_task;

	struct chunk_encoding_task
	{
		chunk_encoding_task(encoding_task& task_, std::size_t chunk_size_);
		~chunk_encoding_task();

		void churn();

		std::size_t fill(io::wav_reader& reader_, std::size_t length_, std::uint64_t order_, std::vector<std::uint8_t>& last_frame_);
		void drain(std::ostream& writer_);

		std::uint64_t order() const 
		{
			return m_order;
		}

	private:
		core::encoding_task& m_master_task;
		std::vector<std::uint8_t> m_buffer;
		std::vector<std::uint8_t> m_last_frame;
		std::vector<std::uint8_t> m_output;
		std::uint64_t m_order;
	};

	inline bool operator < (const chunk_encoding_task& a, const chunk_encoding_task& b)
	{
		return a.order() > b.order();
	}
	inline bool operator > (const chunk_encoding_task& a, const chunk_encoding_task& b)
	{
		return a.order() < b.order();
	}
	inline bool operator <= (const chunk_encoding_task& a, const chunk_encoding_task& b)
	{
		return a.order() >= b.order();
	}
	inline bool operator >= (const chunk_encoding_task& a, const chunk_encoding_task& b)
	{
		return a.order() <= b.order();
	}
	inline bool operator == (const chunk_encoding_task& a, const chunk_encoding_task& b)
	{
		return a.order() == b.order();
	}
	inline bool operator != (const chunk_encoding_task& a, const chunk_encoding_task& b)
	{
		return a.order() != b.order();
	}

}
