#include <core/encoding_task.hpp>
#include <core/chunk_encoding_task.hpp>

#include <cassert>

core::chunk_encoding_task::chunk_encoding_task(encoding_task& task_, std::size_t chunk_size_)
:	m_master_task(task_),
	m_buffer{},
	m_output{}
{
	m_buffer.reserve(chunk_size_);
}

core::chunk_encoding_task::~chunk_encoding_task()
{}

void core::chunk_encoding_task::churn()
{
	io::encoder encoder_(m_master_task.m_layout);

	m_output.clear();

	if (m_last_frame.size() > 0)
	{
		// Must pre-feed the encoder with a bunch of samples from the last frames end, otherwise we get a noticeable gaps/clicks where the buffers join		
		encoder_.sink
		(
			[this](const std::uint8_t* buffer_, std::size_t length_) { },
			m_last_frame.data(),
			m_last_frame.size(),
			encoder_.flags_flush|encoder_.flags_nogap
		);
	}

	encoder_.sink 
	(
	    [this] (const std::uint8_t* buffer_, std::size_t length_) 
	    { 				        
			m_output.insert(m_output.end(), buffer_, buffer_ + length_);			
	    },
	    m_buffer.data(),
	    m_buffer.size(),
	    encoder_.flags_flush|encoder_.flags_nogap
	);

	m_master_task.notify_chunk_complete(*this);
}

std::size_t core::chunk_encoding_task::fill(io::wav_reader& reader_, std::size_t size_, std::uint64_t order_, std::vector<std::uint8_t>& last_frame_)
{
	m_order = order_;	
	m_last_frame = std::move(last_frame_);

	m_buffer.resize(size_);	
	auto loaded_ = reader_.read(m_buffer.data(), m_buffer.size());
	m_buffer.resize(loaded_);

	if (loaded_ >= size_)
	{
		const auto mod_ = m_master_task.m_layout.format().sample_rate > 96000 ? 4 : 2;
		const auto frame_length_ = m_master_task.m_layout.frame_length_in_bytes()*mod_ ;

		assert(m_buffer.size() > frame_length_);

		last_frame_.resize(frame_length_);
		std::copy(m_buffer.begin() + m_buffer.size() - frame_length_, m_buffer.end(), last_frame_.begin());
	}

	return loaded_;
}

void core::chunk_encoding_task::drain(std::ostream& writer_)
{
	writer_.write((const char*)m_output.data(), m_output.size());
}
