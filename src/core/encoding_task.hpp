#pragma once

#include <utils/debug.hpp>
#include <utils/dir_list.hpp>
#include <utils/thread.hpp>
#include <utils/mutex.hpp>
#include <utils/locked_queue.hpp>
#include <utils/thread_pool.hpp>
#include <utils/reffwd.hpp>

#include <io/encoder.hpp>
#include <io/wav_layout.hpp>
#include <io/wav_reader.hpp>

#include <core/chunk_encoding_task.hpp>

namespace core
{

	struct encoding_task
	{
		using completion_handler_type = std::function<void(core::encoding_task*)>;

		friend struct chunk_encoding_task;

		encoding_task(const std::string& src_path_, 
					  const std::string& dst_path_,
					  completion_handler_type notify_);
		~encoding_task();
		encoding_task(const encoding_task&) = delete;
		encoding_task& operator = (const encoding_task&) = delete;
		encoding_task(encoding_task&&);
		encoding_task& operator = (encoding_task&&);

		void churn();

		void notify_chunk_complete(chunk_encoding_task& subtask_);

	private:	
		using chunk_completed_queue_type = utils::locked_queue<std::priority_queue<utils::reffwd<chunk_encoding_task>>>;
		using chunk_task_pool_type = std::vector<chunk_encoding_task>;

		completion_handler_type m_notify;

		std::ifstream m_rd_stream;
		std::ofstream m_wr_stream;

		io::wav_layout m_layout;
		io::wav_reader m_reader;
		
		std::size_t m_chunk_size;
		std::uint64_t m_chunks_loaded;
		std::uint64_t m_chunks_written;
		std::uint32_t m_chunks_pending;

		chunk_task_pool_type m_task_pool;
		chunk_completed_queue_type m_completed;

		std::vector<std::uint8_t> m_last_frame;
	};
}
