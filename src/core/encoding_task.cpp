#include <utils/exchg.hpp>
#include <core/encoding_task.hpp>

#include <cassert>

core::encoding_task::encoding_task(const std::string& src_path_, const std::string& dst_path_, completion_handler_type notify_)
:	m_notify(notify_),
	m_rd_stream(src_path_, std::ios::binary),
	m_wr_stream(dst_path_, std::ios::binary|std::ios::trunc),
	m_layout(m_layout.load(m_rd_stream)),
	m_reader(m_rd_stream, m_layout),
	m_chunk_size(m_layout.milliseconds_to_bytes(5000)),
	m_chunks_loaded(),
	m_chunks_written(),
	m_chunks_pending(),
	m_task_pool(),
	m_completed(),
	m_last_frame(m_layout.frame_length_in_bytes())
{
	const auto hwtc_ = utils::thread::number_of_cores();
	m_task_pool.reserve(hwtc_);
	for (auto i = 0u; i < hwtc_; ++i)
	{
		m_task_pool.emplace_back(*this, m_chunk_size);
	}
}

core::encoding_task::~encoding_task()
{}

core::encoding_task::encoding_task(encoding_task&& o)
:	m_notify (std::move (o.m_notify)),
	m_rd_stream (std::move (o.m_rd_stream)),
	m_wr_stream (std::move (o.m_wr_stream)),
	m_layout (std::move (o.m_layout)),
	m_reader (std::move (o.m_reader)),
	m_chunk_size (utils::exchange(o.m_chunk_size, 0u)),
	m_chunks_loaded  (utils::exchange (m_chunks_loaded,  0u)),
	m_chunks_written (utils::exchange (m_chunks_written, 0u)),
	m_task_pool (std::move (o.m_task_pool)),
	m_completed (std::move (o.m_completed)),
	m_last_frame (std::move (o.m_last_frame))
{}

core::encoding_task& core::encoding_task::operator = (encoding_task&& o)
{
	this->~encoding_task();
	new (this) encoding_task(std::move (o));
	return *this;
}

void core::encoding_task::churn()
{
	// Drain
	while (!m_completed.empty())
	{		
		auto task_ = m_completed.pop().get();
		assert(m_chunks_written == task_.order());		
		--m_chunks_pending;
		++m_chunks_written;
		task_.drain(m_wr_stream);
	}

	// Fill
	std::vector<chunk_encoding_task*> start_;
	start_.reserve(m_task_pool.size());
	for(auto& task_: m_task_pool)
	{
		if (task_.fill(m_reader, m_chunk_size, m_chunks_loaded++, m_last_frame) < 1)
			break;
		start_.push_back(&task_);
		++m_chunks_pending;
	}

	if (start_.size() > 0)
	{
		for (auto task_ : start_)
		{
			utils::dispatch(std::bind(&chunk_encoding_task::churn, task_));
		}
	}
	else
	{
		m_notify(this);
	}
}

void core::encoding_task::notify_chunk_complete(chunk_encoding_task& subtask_)
{
	m_completed.push(subtask_);
	if (m_completed.size() >= m_chunks_pending)
	{
		utils::dispatch(std::bind(&encoding_task::churn, this));
	}
}

