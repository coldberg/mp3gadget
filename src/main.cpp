#include <core/encoding_task.hpp>

#include <random>
#include <cstdlib>
#include <cassert>
#include <thread>
#include <memory>


void tick()
{
	static int spin_ = 0;
	std::cout << ((R"(-\|/)"[spin_++ % 4]) + std::string("\b"));
}

std::string replace_ext(const std::string& path_, const std::string& ext_)
{
	auto it = std::find(path_.rbegin(), path_.rend(), '.');
	if (it != path_.rend())
	{
		return std::string(path_.begin(), it.base()) + ext_;
	}
	return path_;
}

int main(int argc, char** argv)
{	
	using namespace std::chrono;
	if (argc < 2)
	{
		std::cout << "You didn't specify a directory to process ...\n";
		return -1;
	}

	try
	{	
		auto wav_list_ = utils::directory_list(argv[1], "*.wav");
		if (wav_list_.empty())
		{
			std::cout << "Didn't find anything to process ...\n";
			return -1;
		}

		std::shuffle(wav_list_.begin(), wav_list_.end(), std::mt19937_64(std::random_device() ()));

		utils::semaphore<int> break_{ int (std::max<std::size_t>(utils::thread::number_of_cores() / 2, 1)) };
		utils::semaphore<int> done_{ 1 - int (wav_list_.size()) };


		while(!wav_list_.empty())
		{			
			auto wav_name_ = wav_list_.back();
			wav_list_.pop_back();
			try
			{
				while (!break_.wait_for(milliseconds(100)))
				{					
					tick();
				}				

				std::cout << (" \nEncoding " + wav_name_ + " ... ");
				auto task_ = new core::encoding_task
				(
					wav_name_, 
					replace_ext(wav_name_, "mp3"),
					[&break_, &done_] (core::encoding_task* this_)
					{
						break_.post();
						done_.post();
						delete this_;
					}
				);

				utils::dispatch(std::bind(&core::encoding_task::churn, task_));
			}
			catch (const std::exception& ex_)
			{
				break_.post();
				std::cout << ("Unable to encode file " + wav_name_ + " due to reason :\n" + ex_.what() + ".\n");
			}
		}
		while (!done_.wait_for(milliseconds(100)))
		{
			tick();
		}
		return 0;
	}
	catch (const std::exception& ex_)
	{
		std::cout << ex_.what() << "\n";
		return -1;
	}
}

