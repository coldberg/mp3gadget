#pragma once

#include <memory>

namespace io
{
	class wav_chunk
	{
		std::uint64_t m_offset;
		std::uint64_t m_audio_length_in_samples;
	public:
		wav_chunk(std::uint64_t offset_, std::uint64_t length_);

		auto offset() const -> decltype(m_offset) { return m_offset; }
		auto length() const -> decltype(m_audio_length_in_samples) { return m_audio_length_in_samples; }
	};

}