#include <io/wav_layout.hpp>
#include <type_traits>

template <typename _Stream>
inline bool Stream_read_raw(_Stream&& ifs_, void* buff_, std::size_t len_)
{
	ifs_.read((char*)buff_, len_);
	return (std::size_t)ifs_.gcount() == len_;
}

template <typename _Stream, typename _Type>
inline bool Stream_read(_Stream&& ifs_, _Type* buff_, std::size_t len_)
{
	return Stream_read_raw(std::forward<_Stream>(ifs_), buff_, sizeof(_Type) * len_);
}

template <typename _Stream, typename _Type>
inline bool Stream_read(_Stream&& ifs_, _Type& buff_)
{
	static_assert(std::is_pod<_Type>::value, "Must be of POD type");
	return Stream_read(std::forward<_Stream>(ifs_), &buff_, 1u);
}

io::wav_layout io::wav_layout::load(std::istream& stream_)
{
	data_header header_;

	Stream_read(stream_, header_);
	stream_.seekg(0, std::ios::beg);

	Check(stream_.good(), "File corrupt or of incorrect format.");

	auto hdr_check_ = false;
	auto fmt_check_ = false;

	wav_layout wav_;

	while (true)
	{
		if (!Stream_read(stream_, header_))
		{
			break;
		}

		switch (header_.chunk_id)
		{

		case riff_tag::RIFF:
		{
			hdr_check_ = true;
			std::uint32_t format_;
			Check(Stream_read(stream_, format_), "File corrupt or of incorrect format.");
			Check(format_ == riff_tag::WAVE, "File corrupt or of incorrect format.");
			continue;
		}
		case riff_tag::fmt_:
		{
			fmt_check_ = true;
			format_params format_;
			Check(Stream_read(stream_, format_), "File corrupt or of incorrect format.");
			Check(format_.audio_format == 1 || format_.audio_format == 3, "Compressed WAV not supported.");
			Check(format_.num_channels >= 1, "File corrupt or of incorrect format.");
			Check(format_.bits_per_sample >= 1, "File corrupt or of incorrect format.");
			Check(format_.sample_rate >= 1, "File corrupt or of incorrect format.");
			Check(format_.byte_rate >= 1, "File corrupt or of incorrect format.");
			Check(format_.block_align >= 1, "File corrupt or of incorrect format.");
			wav_.m_audio_format = format_;
			stream_.seekg(header_.length - sizeof(format_), std::ios::cur);
			continue;
		}
		case riff_tag::data:
		{
			wav_.m_audio_chunks.emplace_back(stream_.tellg(), header_.length);
			wav_.m_audio_length_in_samples += header_.length;
			stream_.seekg(header_.length, std::ios::cur);
			continue;
		}

		default:
		{
			stream_.seekg(header_.length, std::ios::cur);
			continue;
		}
		}
	}
	Check(hdr_check_ == true, "Header chunk missing.");
	Check(fmt_check_ == true, "Format chunk missing.");
	stream_.clear();
	stream_.seekg(0, std::ios::beg);

	auto lame_ = wav_.build_lame();
	wav_.m_frame_length_in_samples = lame_get_framesize(lame_.get());
	wav_.m_frame_length_in_samples = wav_.m_frame_length_in_samples * 2 + wav_.m_frame_length_in_samples / 2;

	return wav_;
}

auto io::wav_layout::length_in_blocks() const -> std::uint64_t
{
	return m_audio_length_in_samples / m_audio_format.block_align;
}

auto io::wav_layout::length_in_samples() const -> std::uint64_t
{
	return m_audio_length_in_samples / (m_audio_format.bits_per_sample / 8u);
}

auto io::wav_layout::length_in_time() const->std::array<std::uint64_t, 4u>
{
	const auto samples_ = length_in_samples() % m_audio_format.sample_rate;
	const auto seconds_ = length_in_samples() / m_audio_format.sample_rate;
	const auto minutes_ = seconds_ / 60;
	const auto hours_ = minutes_ / 60;

	return std::array<std::uint64_t, 4u> { hours_, minutes_ % 60, seconds_ % 60, samples_ };
}

auto io::wav_layout::sample_size_in_bytes() const -> std::uint32_t
{
	return m_audio_format.bits_per_sample / 8u;
}

auto io::wav_layout::sample_type() const -> typename io::wav_layout::sample_type_enum
{
	switch (m_audio_format.audio_format)
	{
	case 1: 
		switch (m_audio_format.bits_per_sample)
		{
		case  8: return sample_type_enum::int8_type;
		case 16: return sample_type_enum::int16_type;
		case 24: return sample_type_enum::int24_type;
		case 32: return sample_type_enum::int32_type;
		case 64: return sample_type_enum::int64_type;
		default:
			Check(0, "Bad format.");
		}
		break;
	case 3:
		switch (m_audio_format.bits_per_sample)
		{
		case 32: return sample_type_enum::float32_type;
		case 64: return sample_type_enum::float64_type;
		default:
			Check(0, "Bad format.");
		}
		break;
	default:
		Check(0, "Bad format.");
	}

	return sample_type();
}

auto io::wav_layout::block_size_in_bytes() const -> std::uint32_t
{
	return m_audio_format.block_align;
}

auto io::wav_layout::second_size_in_bytes() const -> std::uint32_t
{
	return m_audio_format.byte_rate;
}

auto io::wav_layout::build_lame() const -> lame_ptr_type
{	
	lame_t lame_ = lame_init();
	Check(lame_ != nullptr);
	Check(lame_set_VBR(lame_, vbr_mode::vbr_off) != -1);
	Check(lame_set_brate(lame_, 256) != -1);
	Check(lame_set_in_samplerate(lame_, m_audio_format.sample_rate) != -1);
	Check(lame_set_num_channels(lame_, m_audio_format.num_channels) != -1);

	Check(lame_set_errorf (lame_, [](const char *format, va_list ap) {}) >= 0);
	Check(lame_set_debugf (lame_, [](const char *format, va_list ap) {}) >= 0);
	Check(lame_set_msgf   (lame_, [](const char *format, va_list ap) {}) >= 0);

	Check(lame_init_params(lame_) != -1);
	return lame_ptr_type(lame_, lame_close);
}

auto io::wav_layout::frame_length_in_samples() const -> std::size_t
{	
	return m_frame_length_in_samples;
}

auto io::wav_layout::frame_length_in_bytes() const -> std::size_t
{
	return frame_length_in_samples() * m_audio_format.block_align;
}

auto io::wav_layout::nearest_length_in_frames(std::size_t length_) const -> std::size_t
{
	const auto frame_len_ = frame_length_in_bytes();
	return (length_ + frame_len_ - 1) / frame_len_;
}

auto io::wav_layout::milliseconds_to_bytes(std::size_t millisecs_) const -> std::size_t
{
	return nearest_length_in_frames((millisecs_ * second_size_in_bytes() + 999) / 1000)*frame_length_in_bytes();
}
