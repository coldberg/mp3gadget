#include <io/wav_chunk.hpp>

#include <algorithm>
#include <istream>

io::wav_chunk::wav_chunk(std::uint64_t offset_, std::uint64_t length_) :
	m_offset(offset_),
	m_audio_length_in_samples(length_)
{}

