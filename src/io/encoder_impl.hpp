#pragma once

#include <functional>
#include <cstddef>
#include <cstdint>
#include <algorithm>

#include <io/encoder.hpp>
#include <lame/lame.h>

namespace io
{
	namespace detail
	{
		inline int encode_buffer_interleaved(lame_t lame_, const float data_[], int samples_, std::uint8_t* mp3buf_, int size_)
		{
			return lame_encode_buffer_interleaved_ieee_float(lame_, data_, samples_, mp3buf_, size_);
		}

		inline int encode_buffer_interleaved(lame_t lame_, const double data_[], int samples_, std::uint8_t* mp3buf_, int size_)
		{
			return lame_encode_buffer_interleaved_ieee_double(lame_, data_, samples_, mp3buf_, size_);
		}

		inline float byte_array_to_float(const std::uint8_t(&value_)[1u])
		{
			return static_cast<float>(int(value_[0u]) - 128) * (1.0f / 128.0f);
		}
		inline float byte_array_to_float(const std::uint8_t(&value_)[2u])
		{
			const auto _Mult = 1.0f / 2147483648.0f;
			return static_cast<float>(int(value_[1u] * 0x1000000ul + value_[0u] * 0x10000ul)) * _Mult;
		}
		inline float byte_array_to_float(const std::uint8_t(&value_)[3u])
		{
			const auto _Mult = 1.0f / 2147483648.0f;
			return static_cast<float>(int(value_[2u] * 0x1000000ul + value_[1u] * 0x10000ul + value_[0u] * 0x100ul)) * _Mult;
		}
		inline float byte_array_to_float(const std::uint8_t(&value_)[4u])
		{
			const auto _Mult = 1.0 / 2147483648.0f;
			return static_cast<float>(static_cast<float>(int(value_[3u] * 0x1000000ul + value_[2u] * 0x10000ul + value_[1u] * 0x100ul + value_[0u])) * _Mult);
		}

		inline float byte_array_to_float(const std::uint8_t(&value_)[8u])
		{			
			const auto _Mult = 1.0 / 2147483648.0f;
			return static_cast<float>(static_cast<float>(int(value_[7u] * 0x1000000ul + value_[6u] * 0x10000ul + value_[5u] * 0x100ul + value_[4u])) * _Mult);
		}

	}

	struct encoder_impl
	{
		using sink_function_type = encoder::sink_function_type;

		static const auto flags_flush = encoder::flags_flush;
		static const auto flags_nogap = encoder::flags_nogap;

		virtual void transcode(sink_function_type&& sink, const std::uint8_t* data, std::size_t size, std::uint32_t flags) = 0;
		virtual ~encoder_impl();

		encoder_impl(const wav_layout& wav_layout);

		encoder_impl(const encoder_impl&) = delete;
		encoder_impl& operator = (const encoder_impl&) = delete;

		encoder_impl(encoder_impl&&) = delete;
		encoder_impl& operator = (encoder_impl&&) = delete;

	protected:
		template <typename _Stype>
		void internal_sink(sink_function_type&& sink,
						   const _Stype		    data[],
						   std::size_t			size,
						   std::uint32_t	    flags)
		{
			auto optimal_size_ = size + (size >> 2u) + 7200u;

			if (m_sink_buffer.size() < optimal_size_)
			{
				m_sink_buffer.resize(optimal_size_);
			}

			auto toWrite_ = detail::encode_buffer_interleaved(m_lame.get(), data, (int)size, m_sink_buffer.data(), (int)m_sink_buffer.size());

			if (flags & flags_flush)
			{
				do
				{
					sink(m_sink_buffer.data(), toWrite_);
					auto fun_ = flags & flags_nogap ? lame_encode_flush_nogap : lame_encode_flush;
					toWrite_ = fun_ (m_lame.get(), m_sink_buffer.data(), (int)m_sink_buffer.size());
				} while (toWrite_ > 0);
			}
			else
			{
				sink(m_sink_buffer.data(), toWrite_);
			}
		}

		const wav_layout& m_layout;
		wav_layout::lame_ptr_type m_lame;
		std::vector<std::uint8_t> m_sink_buffer;
	};

	template <typename _Stype>
	struct float_encoder_impl final: 
		public encoder_impl
	{	
		using encoder_impl::encoder_impl;

		void transcode(sink_function_type&& sink, const std::uint8_t* data, std::size_t size, std::uint32_t flags) override
		{
			encoder_impl::internal_sink(std::move(sink), (const _Stype*)data, size / m_layout.block_size_in_bytes (), flags);
		}
	};

	template <std::size_t _SampleSize>
	struct integer_encoder_impl final:
		public encoder_impl
	{
		using encoder_impl::encoder_impl;

		using sample_type = std::uint8_t[_SampleSize];

		void transcode(sink_function_type&& sink, const std::uint8_t* data, std::size_t size, std::uint32_t flags) override
		{

			auto block_length_ = size / m_layout.block_size_in_bytes();
			auto sample_length_ = size / m_layout.sample_size_in_bytes();

			if (m_staging.size() < sample_length_)
			{
				m_staging.resize(sample_length_);
			}

			auto sample_data_ = (const sample_type*)data;

			std::transform
			(
				sample_data_, 
				sample_data_ + sample_length_, 
				m_staging.begin(), 
				[](const sample_type& value_) 
				{
					return detail::byte_array_to_float(value_);
				}
			);

			encoder_impl::internal_sink(std::move(sink), m_staging.data(), block_length_, flags);
		}
	private:
		std::vector<float> m_staging;
	};
}