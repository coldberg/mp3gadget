#include <io/encoder.hpp>
#include <io/encoder_impl.hpp>

io::encoder::encoder(const wav_layout& layout_):
	m_encoder(build_encoder(layout_))
{}

void io::encoder::sink(sink_function_type&& sink, const std::uint8_t* data, std::size_t size, std::uint32_t flags)
{
	(*m_encoder).transcode(std::move(sink), data, size, flags);
}

auto io::encoder::build_encoder(const wav_layout & layout_)
	-> encoder_ptr_type
{
	using deleter_function_type = void(encoder_impl*); 
	 
	deleter_function_type* deleter_ = [](encoder_impl* impl_) -> void { delete impl_; };

	switch (layout_.sample_type())
	{
	case wav_layout::sample_type_enum::float32_type:
		return encoder_ptr_type(new io::float_encoder_impl<float>(layout_), deleter_);
	case wav_layout::sample_type_enum::float64_type:
		return encoder_ptr_type(new io::float_encoder_impl<double>(layout_), deleter_);
	case wav_layout::sample_type_enum::int8_type:
		return encoder_ptr_type(new io::integer_encoder_impl<1u>(layout_), deleter_);
	case wav_layout::sample_type_enum::int16_type:
		return encoder_ptr_type(new io::integer_encoder_impl<2u>(layout_), deleter_);
	case wav_layout::sample_type_enum::int24_type:
		return encoder_ptr_type(new io::integer_encoder_impl<3u>(layout_), deleter_);
	case wav_layout::sample_type_enum::int32_type:
		return encoder_ptr_type(new io::integer_encoder_impl<4u>(layout_), deleter_);
	case wav_layout::sample_type_enum::int64_type:
		return encoder_ptr_type(new io::integer_encoder_impl<8u>(layout_), deleter_);
	default:
		Check(0, "Unsupported sample type.");
	}
	return encoder_ptr_type(nullptr, nullptr);
}
