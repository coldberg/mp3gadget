#pragma once

#include <utils/debug.hpp>
#include <utils/endian.hpp>
#include <io/wav_chunk.hpp>

#include <vector>
#include <array>
#include <fstream>

#include <lame/lame.h>

namespace io
{
	struct wav_layout
	{

#pragma pack(push, 1)
		struct format_params
		{
			std::uint16_t audio_format;
			std::uint16_t num_channels;
			std::uint32_t sample_rate;
			std::uint32_t byte_rate;
			std::uint16_t block_align;
			std::uint16_t bits_per_sample;
		};

		struct data_header
		{
			std::uint32_t chunk_id;
			std::uint32_t length;
		};

		struct format_header
		{
			std::uint32_t chunk_id;
			std::uint32_t length;
			format_params params;
		};

		struct chunk_header
		{
			std::uint32_t chunk_id;
			std::uint32_t length;
			std::uint32_t format;
		};



		enum riff_tag
		{

#if TARGET_IS_LARGE_ENDIAN
			RIFF = 0x52494646,
			WAVE = 0x57415645,
			data = 0x64617461,
			fmt_ = 0x666d7420
#elif TARGET_IS_SMALL_ENDIAN
			RIFF = 0x46464952,
			WAVE = 0x45564157,
			data = 0x61746164,
			fmt_ = 0x20746d66
#endif 

		};

#pragma pack(pop)	

	private:

		format_params			m_audio_format;
		std::vector<wav_chunk>	m_audio_chunks;
		std::uint64_t			m_audio_length_in_samples;
		std::uint64_t			m_frame_length_in_samples;

		wav_layout(): 
			m_audio_format{}, 
			m_audio_chunks{}, 
			m_audio_length_in_samples{0} 
		{}

	public:
		enum sample_type_enum
		{
			int8_type      = 0x108,
			int16_type     = 0x110,
			int24_type	   = 0x118,
			int32_type     = 0x120,
			int64_type     = 0x140,
			float32_type   = 0x220,
			float64_type   = 0x240
		};

		using lame_ptr_type = std::unique_ptr<lame_global_flags, int(*)(lame_t)>;

		static wav_layout load(std::istream& path_);

		auto format() const -> const decltype(m_audio_format)& { return m_audio_format; }
		auto chunks() const -> const decltype(m_audio_chunks)& { return m_audio_chunks; }
		auto length() const -> const decltype(m_audio_length_in_samples)& { return m_audio_length_in_samples; }

		auto length_in_blocks() const -> std::uint64_t;
		auto length_in_samples() const -> std::uint64_t;
		auto length_in_time() const -> std::array<std::uint64_t, 4u>;
		auto sample_size_in_bytes() const -> std::uint32_t;
		auto block_size_in_bytes() const -> std::uint32_t;
		auto second_size_in_bytes() const -> std::uint32_t;
		auto sample_type() const -> sample_type_enum;

		auto build_lame() const -> lame_ptr_type;

		auto frame_length_in_samples() const -> std::size_t;
		auto frame_length_in_bytes() const -> std::size_t;

		auto nearest_length_in_frames(std::size_t) const -> std::size_t;
		auto milliseconds_to_bytes(std::size_t) const -> std::size_t;
	};
}