#pragma once

#include <cstddef>
#include <cstdint>

#include <io/wav_layout.hpp>


namespace io
{
	struct encoder_impl;

	struct encoder
	{			
		encoder(const wav_layout& layout_);

		encoder(const encoder&) = delete;
		encoder(encoder&&) = default;
		encoder& operator = (const encoder&) = delete;
		encoder& operator = (encoder&&) = default;
	    
		~encoder() = default;

		static const auto flags_flush = 0x1;
		static const auto flags_nogap = 0x2;

		using sink_function_type = std::function<void(const std::uint8_t*, std::size_t)>;

		void sink(sink_function_type&&, const std::uint8_t*, std::size_t, std::uint32_t);

	private:		
		using encoder_ptr_type = std::unique_ptr<encoder_impl, void (*)(encoder_impl*)>;

		static auto build_encoder(const wav_layout& layout_) -> encoder_ptr_type;

		encoder_ptr_type m_encoder;
	};
	
}
