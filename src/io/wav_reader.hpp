#pragma once

#include <iostream>

#include <io/wav_layout.hpp>


namespace io
{
	template <typename _Stream = std::istream>
	struct wav_basic_reader
	{
		using float_convert = std::function<std::pair<const std::uint8_t*, std::uint8_t*> (const std::uint8_t*, std::size_t, std::uint8_t*, std::size_t)>;

		wav_basic_reader(std::istream& stream_, const wav_layout& layout_) :
			m_stream(stream_),
			m_layout(layout_),
			m_offset(0u),
			m_ichunk(0u)
		{}

		auto read(std::uint8_t* pbuffer_, std::size_t ulength_) 
			-> std::size_t
		{
			std::size_t currlen_ = 0u;
			const auto& chunks_ = m_layout.chunks();

			while (currlen_ < ulength_ && m_ichunk < chunks_.size())
			{
				const auto& chunk_ = chunks_[m_ichunk];

				auto howlong_ = std::min<std::size_t>(chunk_.length() - m_offset, ulength_);

				if (howlong_ < 1u)
				{				
					if (m_ichunk < chunks_.size() - 1)
					{
						++m_ichunk;
						m_offset = 0;
						continue;
					}
					break;
				}

				m_stream.clear();
				m_stream.seekg(chunk_.offset() + m_offset, std::ios::beg);
				m_stream.read((char*)pbuffer_, howlong_);

				ulength_ -= howlong_;
				pbuffer_ += howlong_;
				m_offset += howlong_;
				currlen_ += howlong_;
			}

			return currlen_;
		}


		auto tell_time() const -> std::array<std::uint64_t, 4u>
		{

			const auto abstime_ = m_offset / m_layout.format().block_align;
			const auto samples_ = abstime_ % m_layout.format().sample_rate;
			const auto seconds_ = abstime_ / m_layout.format().sample_rate;
			const auto minutes_ = seconds_ / 60;
			const auto hours_ = minutes_ / 60;

			return std::array<std::uint64_t, 4u> { hours_, minutes_ % 60, seconds_ % 60, samples_ };
		}

		void rewind()
		{
			m_ichunk = 0;
			m_offset = 0;
		}

	private:

		_Stream&			m_stream;
		const wav_layout&	m_layout;
		std::uint64_t		m_offset;		
		std::uint32_t		m_ichunk;
	};

	using wav_reader = wav_basic_reader<std::istream>;
}