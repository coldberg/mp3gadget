#include <utils/thread.hpp>
#include <utils/mutex.hpp>
#include <utils/condv.hpp>
#include <utils/semaphore.hpp>

#include <cassert>
#include <iostream>

#include <Windows.h>

unsigned utils::thread::number_of_cores()
{	
	//return 1;
	SYSTEM_INFO pi_;
	GetSystemInfo(&pi_);
	return pi_.dwNumberOfProcessors;
}

utils::thread::handle_type utils::thread::current_thread_handle()
{
	return (handle_type)GetCurrentThread();
}

auto utils::thread::start(const attributes_type& attr_, invokable_type invokable_) -> handle_type
{
	using semaphore_type = utils::semaphore<std::size_t>;

	auto frame_ = std::make_pair(semaphore_type{0}, std::ref(invokable_));

	auto handle_ = CreateThread
	(
		nullptr, 
		attr_.m_stack_size, 
		[](void* arg_)
		{ 
			try
			{
				auto& local_ = *(decltype(frame_)*)arg_;
				auto invokable_ = std::move(local_.second);
				local_.first.post();
				invokable_();
			}
			catch (...)
			{}
			return 0ul;
		},
		&frame_,
		0u, 
		nullptr
	);

	frame_.first.wait();	
	Check(handle_ != INVALID_HANDLE_VALUE);

	return (const handle_type&)handle_;
}

void utils::thread::join(handle_type whandle_)
{
	const auto handle_ = (const HANDLE&)whandle_;
	Check(WaitForSingleObject(handle_, INFINITE) == WAIT_OBJECT_0);	
	CloseHandle(handle_);
}
