#include <utils/dir_list.hpp>

#include <string>
#include <algorithm>

#include <Shlwapi.h>
#include <Windows.h>

static std::string win32ize_path(std::string path_, const std::string& wildcard_ = "*.*")
{
	std::transform(path_.begin(), path_.end(), path_.begin(), [](char c) { return c != '/' ? c : '\\'; });
	return path_ + "\\" + wildcard_;
}

static void directory_list(std::vector<std::string>& list_, const std::string& path_, const std::string& wildcard_, std::uint32_t flags_)
{
	const auto recursive_ = !!(flags_ & utils::DIRLIST_SCAN_RECURSIVELY);
	const auto directories_ = !!(flags_ & utils::DIRLIST_INCLUDE_DIRECTORIES);

	WIN32_FIND_DATA fd_;

	auto handle_ = FindFirstFile(win32ize_path(path_).c_str(), &fd_);

	if (handle_ != INVALID_HANDLE_VALUE)
	{
		do
		{			
			auto curr_ = std::string (fd_.cFileName);
			auto full_ = path_ + "/" + curr_;

			if (fd_.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (curr_ == "." || curr_ == "..")
					continue;

				if (directories_ && PathMatchSpec(fd_.cFileName, wildcard_.c_str()))				
					list_.push_back(full_);
				
				if (recursive_)
					directory_list(list_, full_, wildcard_, flags_);
			}
			else
			{
				if (PathMatchSpec(fd_.cFileName, wildcard_.c_str()))
					list_.push_back(full_);
			}
		} 
		while (FindNextFile(handle_, &fd_));

		FindClose(handle_);
	}
}

auto utils::directory_list(const std::string& path_, const std::string& wildcard_, std::uint32_t flags_)
	-> std::vector<std::string>
{
	std::vector<std::string> list_;
	::directory_list(list_, path_, wildcard_, flags_);
	return list_;
}

