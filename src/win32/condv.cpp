#include <utils/condv.hpp>
#include <utils/debug.hpp>

#include <Windows.h>

utils::condv::condv():
	m_internals(new CONDITION_VARIABLE)
{
	InitializeConditionVariable((CONDITION_VARIABLE*)m_internals);
}

utils::condv::~condv() 
{
	if (m_internals != nullptr)
	{
		delete ((CONDITION_VARIABLE*)m_internals);
		m_internals = nullptr;
	}
}

utils::condv::condv(condv&& o):
	m_internals(std::exchange(o.m_internals, nullptr))
{}

utils::condv& utils::condv::operator = (condv&& o)
{
	this->~condv();
	new (this) condv(std::move(o));
	return *this;
}

void utils::condv::wait(utils::mutex& mut_)
{
	Check(m_internals != nullptr);
	SleepConditionVariableCS
	(
		(CONDITION_VARIABLE*)m_internals,
		(CRITICAL_SECTION*)mut_.handle(),
		INFINITE
	);
}

bool utils::condv::wait_until(utils::mutex& mut_, const abstime_type& time_)
{
	using namespace std;
	using namespace std::chrono;

	Check(m_internals != nullptr);

	if (steady_clock::now() >= time_)
	{
		return false;
	}

	auto ms_ = duration_cast<milliseconds>(time_ - steady_clock::now()).count();
	return SleepConditionVariableCS
	(
		(CONDITION_VARIABLE*)m_internals,
		(CRITICAL_SECTION*)mut_.handle(),
		(DWORD)ms_
	);
}

void utils::condv::notify_one()
{
	Check(m_internals != nullptr);
	WakeConditionVariable((CONDITION_VARIABLE*)m_internals);
}

void utils::condv::notify_all()
{
	Check(m_internals != nullptr);
	WakeAllConditionVariable((CONDITION_VARIABLE*)m_internals);
}

void* utils::condv::handle() const
{
	return m_internals;
}
