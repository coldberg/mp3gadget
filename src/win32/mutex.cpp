#include <utils/mutex.hpp>
#include <utils/debug.hpp>

#include <cstring>
#include <utility>
#include <cassert>
#include <new>

#include <Windows.h>

utils::mutex::mutex():
	m_internals(new CRITICAL_SECTION{})
{
	//std::memset(m_internals, 0u, sizeof(CRITICAL_SECTION));
	InitializeCriticalSection((CRITICAL_SECTION*)m_internals);
}

utils::mutex::~mutex()
{
	if (m_internals != nullptr)
	{
		DeleteCriticalSection((CRITICAL_SECTION*)m_internals);
		delete (CRITICAL_SECTION*)m_internals;
		m_internals = nullptr;
	}
}

utils::mutex::mutex(mutex&& from_):
	m_internals(std::exchange(from_.m_internals, nullptr))
{
}

utils::mutex& utils::mutex::operator = (mutex&& from_)
{
	this->~mutex();
	new (this) mutex(std::move(from_));
	return *this;
}

void utils::mutex::lock() const 
{
	Check(m_internals != nullptr);
	EnterCriticalSection((CRITICAL_SECTION*)m_internals);
}

void utils::mutex::unlock() const 
{
	Check(m_internals != nullptr);
	LeaveCriticalSection((CRITICAL_SECTION*)m_internals);
}

bool utils::mutex::try_lock() const
{
	Check(m_internals != nullptr);
	return !!TryEnterCriticalSection((CRITICAL_SECTION*)m_internals);
}

void* utils::mutex::handle() const
{
	return m_internals;
}
