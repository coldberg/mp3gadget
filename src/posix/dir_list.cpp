#include <utils/dir_list.hpp>
#include <utils/debug.hpp>

#include <string>
#include <algorithm>
#include <glob.h>


static void directory_list(std::vector<std::string>& list_, const std::string& path_, const std::string& wildcard_, std::uint32_t flags_)
{
	const auto recursive_ = !!(flags_ & utils::DIRLIST_SCAN_RECURSIVELY);
	const auto adfolders_ = !!(flags_ & utils::DIRLIST_INCLUDE_DIRECTORIES);

	glob_t pglob_ = { 0 };

	auto result_ = glob((path_ + "/" + wildcard_).c_str(), GLOB_MARK, nullptr, &pglob_);

	Check(result_ != GLOB_NOSPACE);
	Check(result_ != GLOB_ABORTED);
	Check(result_ != GLOB_NOMATCH);

	for (auto i = 0u; i < pglob_.gl_pathc; ++i)
	{
		auto full_ = std::string (/*path_ + "/" + */pglob_.gl_pathv[i]);
		if (full_.back () == '/')
		{
			full_.pop_back();
			if (recursive_) directory_list(list_, full_, wildcard_, flags_);
			if (adfolders_) list_.push_back(full_);
			continue;
		}
		list_.push_back(full_);
	}
	globfree(&pglob_);
}

auto utils::directory_list(const std::string& path_, const std::string& wildcard_, std::uint32_t flags_)
	-> std::vector<std::string>
{ 
	std::vector<std::string> list_;
	::directory_list(list_, path_, wildcard_, flags_);
	return list_;
}

