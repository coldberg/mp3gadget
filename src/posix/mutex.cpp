#include <utils/mutex.hpp>
#include <utils/debug.hpp>
#include <utils/exchg.hpp>

#include <cstring>
#include <utility>
#include <cassert>
#include <new>

#include <pthread.h>

utils::mutex::mutex():
	m_internals(new pthread_mutex_t{})
{
	pthread_mutexattr_t attr_;
	Check(pthread_mutexattr_init(&attr_) == 0);
	Check(pthread_mutexattr_settype(&attr_, PTHREAD_MUTEX_RECURSIVE) == 0);
	Check(pthread_mutex_init((pthread_mutex_t*)m_internals, nullptr) == 0);
	Check(pthread_mutexattr_destroy(&attr_) == 0);
}

utils::mutex::~mutex()
{
	if (m_internals != nullptr)
	{
		Check(pthread_mutex_destroy((pthread_mutex_t*)m_internals) == 0);
		delete (pthread_mutex_t*)m_internals;
		m_internals = nullptr;
	}
}

utils::mutex::mutex(mutex&& from_):
	m_internals(utils::exchange(from_.m_internals, nullptr))
{
}

utils::mutex& utils::mutex::operator = (mutex&& from_)
{
	this->~mutex();
	new (this) mutex(std::move(from_));
	return *this;
}

void utils::mutex::lock() const 
{
	Check(m_internals != nullptr);
	Check(pthread_mutex_lock((pthread_mutex_t*)m_internals) == 0);
}

void utils::mutex::unlock() const 
{
	Check(m_internals != nullptr);
	Check(pthread_mutex_unlock((pthread_mutex_t*)m_internals) == 0);
}

bool utils::mutex::try_lock() const
{
	Check(m_internals != nullptr);
	switch (pthread_mutex_trylock((pthread_mutex_t*)m_internals))
	{
		case 0:     
			return true;
		case EBUSY: 
			return false;
		default:
			Check(0);
	}
	return false;
}

void* utils::mutex::handle() const
{
	return m_internals;
}
