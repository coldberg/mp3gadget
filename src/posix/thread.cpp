#include <utils/thread.hpp>
#include <utils/mutex.hpp>
#include <utils/condv.hpp>
#include <utils/semaphore.hpp>

#include <cassert>
#include <iostream>

#include <pthread.h>
#include <sys/sysinfo.h>


unsigned utils::thread::number_of_cores()
{
	return get_nprocs();
}

utils::thread::handle_type utils::thread::current_thread_handle()
{
	return (handle_type)pthread_self();
}

auto utils::thread::start(const attributes_type& in_attr_, invokable_type invokable_) -> handle_type
{
	using semaphore_type = utils::semaphore<std::size_t>;

	auto frame_ = std::make_pair(semaphore_type{0}, std::ref(invokable_));

	pthread_attr_t attr_;
	pthread_attr_init(&attr_);
	//pthread_attr_setstack(&attr_, nullptr, in_attr_.m_stack_size)

	pthread_t handle_;

	Check(pthread_create
	(
		&handle_,
		&attr_,
		[](void* arg_) -> void*
		{ 
			try
			{
				auto& local_ = *(decltype(frame_)*)arg_;
				auto invokable_ = std::move(local_.second);
				local_.first.post();
				invokable_();
			}
			catch (...)
			{}
			return nullptr;
		},
		&frame_
	) == 0);

	pthread_attr_destroy(&attr_);
	frame_.first.wait();

	return (handle_type)handle_;
}

void utils::thread::join(handle_type whandle_)
{
	const auto handle_ = (pthread_t)whandle_;
	void* return_;
	Check(pthread_join(handle_, &return_) == 0);
}
