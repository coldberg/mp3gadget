#include <utils/condv.hpp>
#include <utils/debug.hpp>
#include <utils/exchg.hpp>

#include <chrono>

#include <pthread.h>

utils::condv::condv():
	m_internals(new pthread_cond_t{})
{
	Check(pthread_cond_init((pthread_cond_t*)m_internals, nullptr) == 0);
}

utils::condv::~condv() 
{
	if (m_internals != nullptr)
	{
		pthread_cond_destroy((pthread_cond_t*)m_internals);
		delete (pthread_cond_t*)m_internals;
		m_internals = nullptr;
	}
}

utils::condv::condv(condv&& o):
	m_internals(utils::exchange(o.m_internals, nullptr))
{}

utils::condv& utils::condv::operator = (condv&& o)
{
	this->~condv();
	new (this) condv(std::move(o));
	return *this;
}

void utils::condv::wait(utils::mutex& mut_)
{
	Check(m_internals != nullptr);
	Check(pthread_cond_wait((pthread_cond_t*)m_internals, (pthread_mutex_t*)mut_.handle()) == 0);
}

bool utils::condv::wait_until(utils::mutex& mut_, const abstime_type& time_)
{
	using namespace std;
	using namespace std::chrono;

	Check(m_internals != nullptr);

	if (steady_clock::now() >= time_)
	{
		return false;
	}
	
	auto reltime_ = duration_cast<milliseconds>(time_ - steady_clock::now()).count();

	timespec ts_ = {reltime_ / 1000, (reltime_ % 1000) * 1000000 };


	switch (pthread_cond_timedwait((pthread_cond_t*)m_internals, (pthread_mutex_t*)mut_.handle(), &ts_))
	{
	case ETIMEDOUT:
		return false;
	case 0:
		return true;
	default:
		Check(0);
	}
	return false;
}

void utils::condv::notify_one()
{
	Check(m_internals != nullptr);
	Check(pthread_cond_signal((pthread_cond_t*)m_internals) == 0);
}

void utils::condv::notify_all()
{
	Check(m_internals != nullptr);
	Check(pthread_cond_broadcast((pthread_cond_t*)m_internals) == 0);
}

void* utils::condv::handle() const
{
	return m_internals;
}
