#pragma once

#include <utils/mutex.hpp>
#include <utils/auto_lock.hpp>
#include <utils/condv.hpp>

#include <queue>
#include <set>


namespace utils
{
	namespace details
	{
		template <typename _Value>
		_Value queue_front(std::priority_queue<_Value>& q)
		{
			return q.top();
		}
		template <typename _Value>
		_Value& queue_front(std::queue<_Value>& q)
		{
			return q.front();
		}
		template <typename _Value>
		const _Value& queue_front(const std::priority_queue<_Value>& q)
		{
			return q.top();
		}
		template <typename _Value>
		const _Value& queue_front(const std::queue<_Value>& q)
		{
			return q.front();
		}
	}

	template <typename _Queue>
	struct locked_queue
	{
		using queue_type = _Queue;

		locked_queue():
			m_close(false)
		{}

		locked_queue(locked_queue&& o):
			m_close(std::move(m_close)),
			m_queue(std::move(m_queue)),
			m_mutex(std::move(m_mutex)),
			m_condv(std::move(m_condv))
		{}

		~locked_queue()
		{
			close();
			auto_lock<utils::mutex> _(m_mutex);
		}

		template <typename _Value>
		void push(_Value&& value_)
		{
			{
				utils::auto_lock<utils::mutex> _(m_mutex);
				m_queue.push(std::forward<_Value>(value_));
			}
			m_condv.notify_one ();
		}
				
		auto pop() -> typename _Queue::value_type
		{
			auto_lock<utils::mutex> _(m_mutex);

			if (m_queue.empty())
			{
				m_condv.wait(m_mutex, [this] ()
				{
					return m_close || !m_queue.empty();
				});
			}

			if (m_close || m_queue.empty())
			{
				throw std::underflow_error("Can't pop, probably already destroyed.");
			}

			auto value_ = std::move(details::queue_front(m_queue));
			m_queue.pop();
			return value_;
		}

		template <typename _Value>
		bool try_push(_Value&& value_)
		{
			if (m_mutex.try_lock())
			{
				m_queue.push(std::forward<_Value>(value_));					
				m_mutex.unlock();
				m_condv.notify_one();
				return true;
			}
			return false;
		}

		template <typename _Value>
		bool try_pop(_Value& value_)
		{			
			if (m_mutex.try_lock())
			{
				if (!m_queue.empty())
				{
					value_ = std::move(details::queue_front(m_queue));
					m_queue.pop();
					m_mutex.unlock();
					return true;
				}
				m_mutex.unlock();
			}
			return false;
		}

		bool empty() const
		{
			utils::auto_lock<utils::mutex> _(m_mutex);
			return m_queue.empty();
		}
		std::size_t size() const
		{
			utils::auto_lock<utils::mutex> _(m_mutex);
			return m_queue.size();
		}

		void close()
		{
			{
				auto_lock<utils::mutex> _(m_mutex);
				m_close = true;		
			}
			m_condv.notify_all();
		}

	private:
		volatile bool	m_close; 
		queue_type		m_queue;
		mutable utils::mutex	m_mutex;
		utils::condv	m_condv;
	};
}