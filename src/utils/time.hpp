#pragma once

#include <string>
#include <cstring>
#include <array>

namespace utils
{
	inline std::string format_time(const std::array<std::uint64_t, 4u>& time_)
	{
		char temp_[2048];
		std::sprintf(temp_, "%02llu:%02llu:%02llu:%05llu", time_[0], time_[1], time_[2], time_[3]);
		return temp_;
	}

}
