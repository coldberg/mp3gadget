#pragma once

#include <utils/mutex.hpp>

#include <chrono>
#include <functional>

namespace utils
{
	struct condv
	{
		using abstime_type = std::chrono::time_point<std::chrono::steady_clock>;
		using reltime_type = std::chrono::milliseconds;

		condv();
		~condv();
		condv(const condv&) = delete;
		condv(condv&&);
		condv& operator = (const condv&) = delete;
		condv& operator = (condv&&);

		void wait(utils::mutex&);
		bool wait_until(utils::mutex&, const abstime_type& time_);
		bool wait_for(utils::mutex& lock_, const reltime_type& time_)
		{
			return wait_until(lock_, std::chrono::steady_clock::now() + time_);
		}

		void notify_one();
		void notify_all();

		template <typename _Functor>
		void wait(utils::mutex& lock_, _Functor&& cond_)
		{
			while (!cond_ ())
			{
				wait(lock_);
			}
		}

		template <typename _Functor>
		bool wait_for(utils::mutex& lock_, const reltime_type& time_, _Functor&& cond_)
		{
			return wait_until(lock_, std::chrono::steady_clock::now() + time_, std::forward<_Functor>(cond_));
		}


		template <typename _Functor>
		bool wait_until(utils::mutex& lock_, const abstime_type& time_, _Functor&& cond_)
		{
			while (!cond_ ())
			{
				if (!wait_until(lock_, time_))
					return false;
			}
			return true;
		}

	
		void* handle () const;

	private:
		void* m_internals; 
	};
}