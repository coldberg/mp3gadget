#pragma once

#if defined(__BYTE_ORDER__)

	#define TARGET_IS_SMALL_ENDIAN (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
	#define TARGET_IS_LARGE_ENDIAN (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)

	#if __BYTE_ORDER__ != __ORDER_LITTLE_ENDIAN__ && __BYTE_ORDER__ != __ORDER_BIG_ENDIAN__
		#error Unsupported compiler
	#endif

#elif defined(_MSC_VER) && defined(_WIN32) 
	
	#define TARGET_IS_SMALL_ENDIAN 1
	#define TARGET_IS_LARGE_ENDIAN 0

#else

	#error Unsupported compiler

#endif



