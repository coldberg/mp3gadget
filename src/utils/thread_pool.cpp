#include <utils/thread_pool.hpp>

static const auto _M_loop_count = 48;

utils::thread_pool::thread_pool():
	m_task_slot {0},
	m_should_exit {false}
{
	const auto nhwt_ = utils::thread::number_of_cores();
	m_task_queue.resize(nhwt_);
	m_task_thread.reserve(nhwt_);

	for (auto i = 0u; i < nhwt_; ++i)
	{
		m_task_thread.emplace_back (std::bind(&thread_pool::poll, this, i));
	}
}

void utils::thread_pool::poll(unsigned id_)
{
	try
	{
		task_entry_type task_;
		const auto qsize_ = m_task_queue.size();
		const auto count_ = _M_loop_count * qsize_;
	retry_:
		while (!m_should_exit)
		{		
			for (auto i = 0u; i < count_; ++i)
			{
				if (!m_task_queue[(i + id_) % qsize_].try_pop(task_))
				{
					continue;
				}

				task_();
				goto retry_;
			}

			task_ = m_task_queue[id_].pop();
			task_();
		}
	}
	catch (const std::exception&)
	{}
}

utils::thread_pool::~thread_pool()
{
	m_should_exit = true;
	for (auto& q_ : m_task_queue)
	{
		q_.close();
	}
}

void utils::thread_pool::dispatch(task_entry_type task_)
{
	auto qsize_ = m_task_queue.size();
	auto count_ = qsize_ * _M_loop_count;

	auto nextq_ = m_task_slot++;

	for (auto i = 0u; i < count_; ++i)
	{
		if (!m_task_queue[(i + nextq_) % qsize_].try_push(std::move(task_)))
		{
			continue;
		}

		return;
	}
	
	m_task_queue[nextq_ % qsize_].push(std::move(task_));
}

utils::thread_pool& utils::thread_pool::global()
{
	static utils::thread_pool inst_;
	return inst_;
}
