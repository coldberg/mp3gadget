#pragma once

#include <cstdint>
#include <cstddef>

namespace utils
{

	struct mutex
	{
		mutex();
		~mutex();
		mutex (const mutex&) = delete;
		mutex (mutex&&);
		mutex& operator = (const mutex&) = delete;
		mutex& operator = (mutex&&);

		void lock() const;
		void unlock() const;

		bool try_lock() const;

		void* handle() const;

	private:
		void* m_internals;
	};

}