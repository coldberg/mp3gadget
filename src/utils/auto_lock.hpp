#pragma once

#include <stdexcept>

namespace utils
{
	template <typename _Mutex>
	struct auto_lock
	{
		auto_lock() = delete;
		auto_lock(const auto_lock&) = delete;
		auto_lock(auto_lock&&) = delete;
		auto_lock& operator = (const auto_lock&) = delete;
		auto_lock& operator = (auto_lock&&) = delete;

		auto_lock(_Mutex& mutex_) : 
			m_mutex(mutex_) 
		{ 
			m_mutex.lock(); 
		}

		~auto_lock() 
		{ 
			m_mutex.unlock(); 
		}

	private:
		_Mutex& m_mutex;
	};


	template <typename _Mutex>
	struct auto_try_lock
	{
		auto_try_lock() = delete;
		auto_try_lock(const auto_try_lock<_Mutex>&) = delete;
		auto_try_lock(auto_try_lock<_Mutex>&&) = delete;
		auto_try_lock& operator = (const auto_try_lock<_Mutex>&) = delete;
		auto_try_lock& operator = (auto_try_lock<_Mutex>&&) = delete;

		auto_try_lock(_Mutex& mutex_) :
			m_mutex(mutex_),
			m_locked(m_mutex.try_lock())
		{
		}

		~auto_try_lock()
		{
			if (m_locked)
			{
				m_mutex.unlock();
			}
		}

		bool owns_lock() const
		{
			return m_locked;
		}

	private:
		_Mutex& m_mutex;
		bool m_locked;
	};

}
