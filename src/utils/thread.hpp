#pragma once

#include <cstddef>
#include <cstdint>

#include <functional>
#include <utility>

#include <utils/debug.hpp>
#include <utils/exchg.hpp>

namespace utils
{


	class thread
	{		
		using invokable_type = std::function<void()>;

	public:
		using handle_type = std::uintptr_t;

		struct attributes_type
		{
			attributes_type(): m_stack_size(2 * 1024 * 1024) {}

			std::size_t m_stack_size;
		};

		thread():
			m_handle{ handle_type{} }
		{}

		template <typename _Invokable>	
		thread(_Invokable&& invokable_, const attributes_type& attr_ = attributes_type{}):
			m_handle(start(attr_, std::forward<_Invokable>(invokable_)))
		{		
		}

		thread(const thread&) = delete;

		thread(thread&& prev_):
			m_handle(utils::exchange(prev_.m_handle, handle_type{}))
		{}

		thread& operator = (const thread&) = delete;

		thread& operator = (thread&& prev_)
		{
			this->~thread();
			new (this) thread(std::move(prev_));
			return *this;
		}

		~thread()
		{
			if (m_handle != handle_type{})
			{
				join();
			}
		}

		void detach()
		{
			m_handle = handle_type{};
		}

		void join()
		{
			join(utils::exchange(m_handle, handle_type{}));
		}


		handle_type handle() const 
		{
			return m_handle;
		}

		static unsigned number_of_cores();
		static handle_type current_thread_handle();

	private:
		static handle_type start(const attributes_type&, invokable_type invokable);
		static void join(handle_type handle_);

		handle_type m_handle;
	};

	

}