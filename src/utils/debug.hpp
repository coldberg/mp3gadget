#pragma once

#include <functional>
#include <cstring>
#include <stdexcept>

namespace debug
{
	struct Exception : public std::runtime_error
	{
		using std::runtime_error::runtime_error;
	};

	template <typename _Exception, typename... _Args>
	void Check_(bool condition_, _Args&&... args_)
	{
		if (!condition_)
		{
			throw _Exception(std::forward<_Args>(args_)...);
		}
	}
}

#define Literalize_Impl(X) #X
#define Literalize(X) Literalize_Impl(X)
#define CheckEx(Exception, Condition, ...) debug::Check_<Exception>(Condition, __FILE__ ":" Literalize(__LINE__) ", assertion failed : " #Condition ".\n" __VA_ARGS__)
#define Check(Condition, ...)       debug::Check_<debug::Exception>(Condition, __FILE__ ":" Literalize(__LINE__) ", assertion failed : " #Condition ".\n" __VA_ARGS__)
#define PrintExpression(Stream, Exp) Stream << #Exp " = " << (Exp)
#define PrintExpressionLn(Stream, Exp) Stream << #Exp " = " << (Exp) << "\n"