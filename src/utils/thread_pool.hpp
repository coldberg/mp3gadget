#pragma once

#include <queue>
#include <functional>

#include <utils/mutex.hpp>
#include <utils/semaphore.hpp>
#include <utils/thread.hpp>
#include <utils/locked_queue.hpp>

namespace utils
{
	struct thread_pool
	{
		using task_entry_type = std::function<void()>;

		thread_pool();
		~thread_pool();

		void dispatch(task_entry_type);

		static thread_pool& global();
	
	private:
		void poll(unsigned i);
		
		using task_queue_type = utils::locked_queue<std::queue<task_entry_type>>;

		std::vector<task_queue_type>  m_task_queue;
		std::vector<utils::thread>	  m_task_thread;

		// Could be atomic but we don't care as much for strict correctness here

		volatile std::uintptr_t		  m_task_slot;
		volatile bool				  m_should_exit;
	};

	template<typename _Execute>
	void dispatch(_Execute&& exec_)
	{
		utils::thread_pool::global().dispatch(std::forward<_Execute>(exec_));
	}
}
