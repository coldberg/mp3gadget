#pragma once

#include <cstddef>
#include <cstdint>

#include <limits>
#include <algorithm>
#include <new>

#include <utils/mutex.hpp>
#include <utils/condv.hpp>
#include <utils/auto_lock.hpp>
#include <utils/exchg.hpp>

namespace utils
{

	template <typename _Counter = std::size_t>
	struct semaphore
	{
		using value_type = _Counter;
		using reltime_type = condv::reltime_type;
		using abstime_type = condv::abstime_type;
	
		void wait() 
		{
			utils::auto_lock<utils::mutex> _(m_mutex);
			if (m_value <= 0)
			{
				m_condv.wait(m_mutex, [this] () 
				{
					return m_value > 0;
				});
			}

			--m_value;
		}

		bool wait_until(abstime_type time_)
		{
			utils::auto_lock<utils::mutex> _(m_mutex);
			auto result_ = m_condv.wait_until(m_mutex, time_, [this]()
			{
				return m_value > 0;
			});
			if (result_ == true)
			{
				--m_value;
				return true;
			}
			return false;
		}

		bool wait_for(reltime_type ms_)
		{
			return wait_until(std::chrono::steady_clock::now() + ms_);		
		}

		void post()
		{
			{
				utils::auto_lock<utils::mutex> _(m_mutex);
				++m_value;
			}
			m_condv.notify_one();
		}

		semaphore(const value_type& init_ = value_type{})
		:	m_value(init_)
		{}

		semaphore(const semaphore<value_type>&) = delete;
		semaphore& operator = (const semaphore<value_type>&) = delete;

		semaphore(semaphore<value_type>&& o):
			m_value(utils::exchange(o.m_value, value_type{})),
			m_mutex(std::move(o.m_mutex)),
			m_condv(std::move(o.m_condv))
		{}

		semaphore& operator = (semaphore<value_type>&& o)
		{
			this->~semaphore();
			new (this) semaphore(std::move(o));
			return *this;
		}

	private:
		volatile value_type m_value;
		utils::mutex m_mutex;
		utils::condv m_condv;
	};


}

