#pragma once

#include <string>
#include <vector>
#include <algorithm>

namespace utils
{
	static const auto DIRLIST_INCLUDE_DIRECTORIES = 0x1u;
	static const auto DIRLIST_SCAN_RECURSIVELY = 0x2u;

	auto directory_list(const std::string& path, const std::string& wildcard = "*.*", std::uint32_t flags = 0u) -> std::vector<std::string>;

}