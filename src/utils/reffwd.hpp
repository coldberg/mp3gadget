#pragma once

namespace utils
{
	template <typename T>
	struct reffwd
	{
		reffwd(): m_fwd(nullptr){}
		reffwd(T& fwd_): m_fwd(&fwd_){}
		reffwd(const reffwd<T>& o): m_fwd(o.m_fwd) {}
		reffwd<T>& operator = (const reffwd<T>& o)
		{
			m_fwd = o.m_fwd;
			return *this;
		}

		T& get() { return *m_fwd; }
		T& get() const { return *m_fwd; }

	private:
		T* m_fwd;
	};

	template <typename T>
	bool operator < (const reffwd<T>& a, const reffwd<T>& b)
	{
		return a.get() < b.get();
	}
	template <typename T>
	bool operator > (const reffwd<T>& a, const reffwd<T>& b)
	{
		return a.get() > b.get();
	}
	template <typename T>
	bool operator == (const reffwd<T>& a, const reffwd<T>& b)
	{
		return a.get() == b.get();
	}
	template <typename T>
	bool operator <= (const reffwd<T>& a, const reffwd<T>& b)
	{
		return a.get() <= b.get();
	}
	template <typename T>
	bool operator >= (const reffwd<T>& a, const reffwd<T>& b)
	{
		return a.get() >= b.get();
	}
	template <typename T>
	bool operator != (const reffwd<T>& a, const reffwd<T>& b)
	{
		return a.get() != b.get();
	}
}
